#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Criado em 2019-09-14 16:00:17

Autor: Gilvan Farias da Silva
'''
from copy import deepcopy

import numpy as np

from particula import Particula


class PSO:
    def __init__(self,
                 dimensao_tuple = (3,3,3,),
                 num_particulas_int = 50,
                 max_iteracoes_int = 30,
                 mensagem_bool = True,
                 w_min_float = 0.4,
                 w_max_float = 0.9,
                 c1_float = 1,
                 c2_float = 1,
                 velocidade_minima_float = -0.5,
                 velocidade_maxima_float = 0.5,
                 posicao_minima_float = -15, 
                 posicao_maxima_float = 15,
                 inicializacao = 'nguyen',
                 funcao_ativacao_escondida = 'tanh',  
                 funcao_ativacao_final = 'tanh',
                 taxa_reinicializacao_float = 0.2):
        '''
        Inicializa enxame e executa algoritmo PSO
        
        Parâmetros
        ==========
        obj_func : function
            Função com a lógica para calcular o  valor fitness. 
            Deve ter os seguintes parâmetros:
                (self.posicao_list, entrada_array, real_array, iteracao_int)
            Deve retornar o seguinte:
                [valor_fitness_float, matriz_confusao_array]
        entrada_array : array
            Entrada da rede neural
        real_array : array
            Saída real da rede neural
        dimensao_tuple : tuple
            Tupla com dimensões da rede neural
        
        Parâmetros opcionais
        ====================        
        num_particulas_int : int
            Número de partículas no enxame
        max_iteracoes_int : int
            Número máximo de iterações
        mensagem_bool : bool
            Define se mensagens serão exibidas durante a execução do algoritmo
        velocidade_minima_float : float
            Valor da menor componente de velocidade
        velocidade_maxima_float : float
            Valor da maior componente de velocidade
        ''' 

        self.dimensao_tuple = dimensao_tuple
        self.num_particulas_int = num_particulas_int
        self.max_iteracoes_int = max_iteracoes_int
        self.mensagem_bool = mensagem_bool
        self.w_min_float = w_min_float
        self.w_max_float = w_max_float
        self.c1_float = c1_float
        self.c2_float = c2_float
        self.velocidade_minima_float = velocidade_minima_float
        self.velocidade_maxima_float = velocidade_maxima_float
        self.posicao_minima_float = posicao_minima_float
        self.posicao_maxima_float = posicao_maxima_float
        self.inicializacao = inicializacao
        self.funcao_ativacao_escondida = funcao_ativacao_escondida
        self.funcao_ativacao_final = funcao_ativacao_final
        self.taxa_reinicializacao_float = taxa_reinicializacao_float
        
        self.historico = list()
        
        
        
        
        self.acuracia_treino_list = list()
        self.pe_treino_list = list()
        self.eqm_treino_list = list()
        self.entropia_treino_list = list()
        self.conf_sd_treino_list = list()
        self.acuracia_validacao_list = list()
        self.pe_validacao_list = list()
        self.eqm_validacao_list = list()
        self.entropia_validacao_list = list()
        self.conf_sd_validacao_list = list()
        self.enxame_list = list()  
        
        self.g_best = Particula(self.dimensao_tuple,
                                self.velocidade_minima_float,                 
                                self.velocidade_maxima_float)
        
        self._mensagem('Inicializando enxame!\n')
            
        # Inicializa enxame
        for p in range(self.num_particulas_int):
            self.enxame_list.append(Particula(self.dimensao_tuple,
                                              self.velocidade_minima_float,                 
                                              self.velocidade_maxima_float))
   
        
    def otimizar(self, 
                 objetivo,
                 entrada_treino, 
                 desejado_treino): 
                 #entrada_validacao,
                 #desejado_validacao):        
        self._mensagem('Otimizando!\n')
        
            
        # Para cada iteração
        for iteracao in range(self.max_iteracoes_int):  
            
            w_float = (self.max_iteracoes_int - iteracao) * \
            (self.w_max_float - self.w_min_float)/self.max_iteracoes_int + \
            self.w_min_float

            # Move Particulas
            for p in range(self.num_particulas_int):                                    
                self.enxame_list[p].atualiza_posicao(\
                                self.g_best.p_best_posicao_list,
                                w_float,
                                self.c1_float,
                                self.c2_float)
                
            # Calcula indicadores
            for p in range(self.num_particulas_int):
                self.enxame_list[p].calcula_indicadores(entrada_treino, 
                                                        desejado_treino)
            
                
            # Função objetivo de todas as partículas
            for p in range(self.num_particulas_int):
                self.enxame_list[p] = objetivo(self.enxame_list[p], iteracao)
                
            # Atualiza fitness
            for p in range(self.num_particulas_int):
                self.enxame_list[p].atualiza_fitness()
            
            # Atualiza o g best
            self.atualiza_g_best()
            
            # Reinicia particulas
            for p in range(self.num_particulas_int):                    
                reinit = np.random.rand()
                if reinit < self.taxa_reinicializacao_float:
                    if self.enxame_list[p] != self.g_best:
                        self.enxame_list[p].reinicia()
                        
                        
                        
                        
                        
                        
                        
                        
            # Calcula indicadores
            for p in range(self.num_particulas_int):
                self.enxame_list[p].calcula_indicadores(entrada_treino, 
                                                        desejado_treino)
            
                
            # Função objetivo de todas as partículas
            for p in range(self.num_particulas_int):
                self.enxame_list[p] = objetivo(self.enxame_list[p], iteracao)
                
            # Atualiza fitness
            for p in range(self.num_particulas_int):
                self.enxame_list[p].atualiza_fitness()
            
            # Atualiza o g best
            self.atualiza_g_best()
            
            
            
            
            
            
            
            
            '''
            # backpropagation Particulas
            for p in range(self.num_particulas_int):
                if np.all(np.equal(self.enxame_list[p].p_best_posicao_list[0], self.g_best.p_best_posicao_list[0])):
                    for i in range(5):
                        self.enxame_list[p].retropropaga(entrada_treino, desejado_treino)
            
            
            # Calcula indicadores
            for p in range(self.num_particulas_int):
                self.enxame_list[p].calcula_indicadores(entrada_treino, 
                                                        desejado_treino)
            
                
            # Função objetivo de todas as partículas
            for p in range(self.num_particulas_int):
                self.enxame_list[p] = objetivo(self.enxame_list[p], iteracao)
                
            # Atualiza fitness
            for p in range(self.num_particulas_int):
                self.enxame_list[p].atualiza_fitness()
            
            # Atualiza o g best
            self.atualiza_g_best()
            '''
            
            
            
            
            
            
            
            
            
            

            
            #Calcula indicadores de treino aqui            
            self.g_best.calcula_indicadores_best(entrada_treino,
                                            desejado_treino)
            self.acuracia_treino_list.append(self.g_best.acuracia_float)
            self.pe_treino_list.append(self.g_best.pe_float)
            self.eqm_treino_list.append(self.g_best.eqm_float)
            self.entropia_treino_list.append(self.g_best.entropia_float)

            
            
            
            '''
            #Calcula indicadores de validacao aqui            
            self.g_best.calcula_indicadores_best(entrada_validacao, 
                                            desejado_validacao)
            self.acuracia_validacao_list.append(self.g_best.acuracia_float)
            self.pe_validacao_list.append(self.g_best.pe_float)
            self.eqm_validacao_list.append(self.g_best.eqm_float)
            self.entropia_validacao_list.append(self.g_best.entropia_float)
            self.conf_sd_validacao_list.append(self.g_best.conf_sd_float)
            '''
            
            self.historico.append(deepcopy(self.g_best))

            self._mensagem('It = '+str(iteracao)+' Fitness = '+str(self.g_best.p_best_fitness_float))
            #print(self.g_best.p_best_posicao_list)
        self._mensagem('Fim\n')
     
        
    def atualiza_g_best(self):
        for p in range(self.num_particulas_int):
            if self.enxame_list[p].p_best_fitness_float < self.g_best.p_best_fitness_float:
                #print('atualiza_g_best', self.enxame_list[p].p_best_fitness_float, self.g_best.p_best_fitness_float)
                self.g_best = deepcopy(self.enxame_list[p])




    def _mensagem(self, msg_str):
        if (self.mensagem_bool):
            print(msg_str)
            
            
    
    def predizer(self, entrada_array):
        Y = self.g_best.propagar(entrada_array)
        return Y





            
            
            
