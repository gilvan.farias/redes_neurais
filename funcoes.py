#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Criado em 2019-09-13 16:40:17

Autor: Gilvan Farias da Silva
'''

import numpy as np



def binario(x):
    # Não funciona com array
    if x >= 0:
        return 1
    else:
        return 0

def identidade(x):
    return x

def logistica(x):
    return 1.0 / (1.0 + np.exp(-x))

def tanh(x):
    return np.tanh(x)

def relu(x):
    # Não funciona com array
    return np.max([0,x])

def leakyrelu(a, x):
    if x >= 0:
        return x
    else:
        return a*x

def softmax(entrada_array):
    '''
    Calcula a função softmax
    
    Parâmetros:
    ===========
    entrada_array : array
        Matriz com valores de entrada. Cada linha equivale a uma leitura
    
    Retorna: 
    ========
    saida_array : array
        Resultado da função softmax
    '''

    if len(entrada_array.shape) == 2:
        saida_array = np.zeros(entrada_array.shape)        
        for i in range(entrada_array.shape[0]):
            saida_array[i] = np.exp(entrada_array[i])/np.sum(np.exp(entrada_array[i]))
        
    elif len(entrada_array.shape) == 1:
        saida_array = np.exp(entrada_array)/np.sum(np.exp(entrada_array))    
    
    return saida_array




ATIVACAO = {'identidade': identidade,
            'tanh': tanh, 
            'logistica': logistica,
            'softmax': softmax}



def derivada_identidade(x):
    return 1.0

def derivada_tanh(x):
    return (1.0 - (np.tanh(x))**2)

def derivada_logistica(x):
    return logistica(x) * (1.0 - logistica(x))
    #return (1.0/np.cosh(x))**2


DERIVADA = {'identidade': derivada_identidade,
               'tanh': derivada_tanh,
               'logistica': derivada_logistica}
