#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Criado em 2019-09-10 19:00:17

Autor: Gilvan Farias da Silva
'''

import numpy as np
from scipy.sparse import coo_matrix

import sys



def nome_funcao():
    '''
    Ao executar esta função dentro de outra função é retornado o nome da 
    função que fez a chamada. Util para determinar a orígem de um erro 
    durante a execução do programa.
    
    Retorna: 
    ========
    nome_str : str   
        Nome da função que fez a chamada
    '''
    
    nome_str = sys._getframe(1).f_code.co_name
    
    return nome_str
    
    
    
def matriz_confusao(real_array, predito_array):
    '''
    Calcula a matriz de confusão normalizada.
    As linhas correspondem à classe real e 
    as colunas correspondem à classe predita
    
    Parâmetros:
    ===========
    real_array : array
        Array com valores reais
    predito_array : array
        Array com valores preditos
    
    Retorna: 
    ========
    matriz_confusao_array : array    
        A matriz de confusão normalizada
    '''
    
    if real_array.shape != predito_array.shape:
        print('Erro na função ',nome_funcao(),'!\n')
        print('real_array.shape = ', real_array.shape,'\n')
        print('predito_array.shape = ', predito_array.shape,'\n')
        return ('Erro')
    else:
        
        indice_real_array = np.argmax(real_array, axis=1)
        indice_predito_array = np.argmax(predito_array, axis=1)
        
        pesos_array = np.ones(len(real_array))
        
        matriz_confusao_array = coo_matrix((pesos_array, (indice_real_array, indice_predito_array)),
                    shape=(len(real_array[0]), len(real_array[0])), dtype=np.float64).toarray()
        
        matriz_confusao_array = matriz_confusao_array.astype('float') / \
                        matriz_confusao_array.sum(axis=1)[:, np.newaxis]

        return (matriz_confusao_array)   
    
    
    
def acuracia(matriz_confusao_array):
    '''
    Calcula a acurácia de uma matrix de confusão
    
    Parâmetros:
    ===========
    matriz_confusao_array : array
        Matriz de confusão
    
    Retorna: 
    ========
    acuracia_float : float
        A acurácia
    '''
    
    if matriz_confusao_array.shape[0] != matriz_confusao_array.shape[1]:
        print('Erro na função ',nome_funcao(),'!\n')
        print('matriz_confusao_array.shape[0] = ', 
              matriz_confusao_array.shape[0],'\n')
        print('matriz_confusao_array.shape[1] = ',
              matriz_confusao_array.shape[1],'\n')
        return ('Erro')
    else:
        soma_float = 0.0
        for i in range(len(matriz_confusao_array)):
            soma_float += matriz_confusao_array[i,i]
        acuracia_float = soma_float/np.sum(matriz_confusao_array)
        
        return acuracia_float
    
    
    
def produto_eficiencias(matriz_confusao_array):
    '''
    Calcula o produto das eficiências de uma matrix de confusão
    
    Parâmetros:
    ===========
    matriz_confusao_array : array
        Matriz de confusão
    
    Retorna: 
    ========
    produto_eficiencias_float : float
        O produto das eficiências
    '''
    
    if matriz_confusao_array.shape[0] != matriz_confusao_array.shape[1]:
        print('Erro na função ',nome_funcao(),'!\n')
        print('matriz_confusao_array.shape[0] = ', 
              matriz_confusao_array.shape[0],'\n')
        print('matriz_confusao_array.shape[1] = ',
              matriz_confusao_array.shape[1],'\n')
        return ('Erro')
    else:
        produto_float = 1.0
        for i in range(len(matriz_confusao_array)):
            produto_float = produto_float * matriz_confusao_array[i,i]
        produto_eficiencias_float = produto_float** \
                                    (1.0/len(matriz_confusao_array))
        
        return produto_eficiencias_float
        
    
    
def confusao_total(matriz_confusao_array):
    '''
    Calcula a confusão total de uma matrix de confusão
    
    Parâmetros:
    ===========
    matriz_confusao_array : array
        Matriz de confusão
    
    Retorna: 
    ========
    confusão_float : float
        A confusão total
    '''
    
    confusao_float = np.sum(matriz_confusao_array)
    for i in range(len(matriz_confusao_array)):
        confusao_float = confusao_float - matriz_confusao_array[i,i]
    return confusao_float



def confusao_sd(matriz_confusao_array):
    '''
    Calcula a confusão da classe sd
    
    Parâmetros:
    ===========
    matriz_confusao_array : array
        Matriz de confusão
    
    Retorna: 
    ========
    confusao_sd : float
        A confusão da classe sd
    '''
    
    if matriz_confusao_array.shape[0] != matriz_confusao_array.shape[1]:
        print('Erro na função ',nome_funcao(),'!\n')
        print('matriz_confusao_array.shape[0] = ', 
              matriz_confusao_array.shape[0],'\n')
        print('matriz_confusao_array.shape[1] = ',
              matriz_confusao_array.shape[1],'\n')
        return ('Erro')
    else:
        soma_float = matriz_confusao_array[1,0] + matriz_confusao_array[2,0]
        confusao_sd_float = soma_float/np.sum(matriz_confusao_array)
        
        return confusao_sd_float



def erro_quadratico_medio(desejado_array, predito_array):
    '''
    Calcula o erro quadrático médio entre o valor predito pela rede neural
    e o valor desejado.
    
    Parâmetros:
    ===========
    desejado_array : array
        Matriz com valores desejados na saída da rede neural. Cada linha
        equivale a uma leitura em que as colunas representam os neurônios de 
        saída da rede neural.
        
    predito_array : array
        Matriz com valores preditos pela rede neural. Cada linha equivale
        a uma leitura em que as colunas representam os neurônios de saída 
        da rede neural.  
    
    Retorna: 
    ========
    eqm_float : float
        O erro quadrático médio
    '''
    
    if desejado_array.shape != predito_array.shape:
        print('Erro na função ',nome_funcao(),'!\n')
        print('Matrizes com shape diferentes')
        return ('Erro')
    else:
        erro_array = desejado_array - predito_array
        erro2_array = erro_array*erro_array
        soma_float = erro2_array.sum()/2
        
        if len(predito_array.shape) == 1:
            eqm_float = soma_float
        elif len(predito_array.shape) == 2:
            eqm_float = soma_float/(predito_array.shape[0])
        
    return eqm_float



def cross_entropia(desejado_array, predito_array):
    '''
    Calcula a entropia cruzada entre o valor predito pela rede neural
    e o valor desejado.
    
    Parâmetros:
    ===========
    desejado_array : array
        Matriz com valores desejados na saída da rede neural. Cada linha
        equivale a uma leitura em que as colunas representam os neurônios de 
        saída da rede neural.
        
    predito_array : array
        Matriz com valores preditos pela rede neural. Cada linha equivale
        a uma leitura em que as colunas representam os neurônios de saída 
        da rede neural.  
    
    Retorna: 
    ========
    c_ent_float : float
        A entropia cruzada
    '''
    
    if desejado_array.shape != predito_array.shape:
        print('Erro na função ',nome_funcao(),'!\n')
        print('Matrizes com shape diferentes')
        return ('Erro')
    else:
        termo_array = desejado_array*np.log10(predito_array)
        c_ent_float = -(termo_array.sum())
        
    return c_ent_float



def cross_entropia_binaria():
    pass



def uniforme(linhas_int, colunas_int, inferior_float, superior_float):
    '''
    Cria uma matriz de dimensão (linhas_int X colunas_int) com números 
    aleatórios uniformemente distribuídos entre inferior_float e superior_float
    
    Parâmetros:
    ===========
    inferior_float : float
        limite inferior dos números aleatórios
    
    superior_float : float
        limite superior dos números aleatórios
    
    Retorna: 
    ========
    W_array : array
        Matriz com números aleatórios
    '''
    
    intervalo_float = superior_float - inferior_float
    W_array = np.random.rand(linhas_int, colunas_int)*intervalo_float +\
                np.ones([linhas_int, colunas_int])*inferior_float
    
    return W_array
    
    
    
def init_nguyen(dimensao_tuple):
    '''
    Inicializa uma rede neural pelo critério de Nguyen Widrow
    
    Parâmetros:
    ===========
    dimensao_tuple : tuple
        Tupla com dimensões da rede neural
        
    Retorna
    =======
    rede_list : list
        Lista com matrizes da rede neural
    '''
    
    rede_list = list()
    
    for i in range(len(dimensao_tuple) - 1):
        n = dimensao_tuple[i]
        p = dimensao_tuple[i+1]
        beta = 0.7*(p**(1/n))
        W_array = uniforme(p, n, -0.5, 0.5)
        sj = ((W_array**2).sum(axis = 1)[:,None])**(1/2)
        W_array = (beta*W_array)/sj
        B_array = uniforme(p, 1, -beta, beta)
        rede_list.append(W_array)
        rede_list.append(B_array)           

    return rede_list



def init_glorot(dimensao_tuple):
    '''
    Inicializa uma rede neural pelo critério de Xavier Glorot
    
    Parâmetros:
    ===========
    dimensao_tuple : tuple
        Tupla com dimensões da rede neural
        
    Retorna
    =======
    rede_list : list
        Lista com matrizes da rede neural
    '''
    
    rede_list = list()
    
    for i in range(len(dimensao_tuple) - 1):   
        limite_float = np.sqrt(6/(dimensao_tuple[i] + dimensao_tuple[i+1]))
        W_array = uniforme(dimensao_tuple[i+1], dimensao_tuple[i],
                                -limite_float, limite_float)
        B_array = np.zeros(dimensao_tuple[i+1])[:,None]    
        rede_list.append(W_array)
        rede_list.append(B_array) 

    return rede_list

    
    
def init_he(dimensao_tuple):
    '''
    Inicializa uma rede neural pelo critério de Xavier Glorot
    
    Parâmetros:
    ===========
    dimensao_tuple : tuple
        Tupla com dimensões da rede neural
        
    Retorna
    =======
    rede_list : list
        Lista com matrizes da rede neural
    '''
    
    rede_list = list()
    pass
    
    
    
def init_velocidade(dimensao_tuple):
    '''
    Inicializa uma rede neural pelo critério de Xavier Glorot
    
    Parâmetros:
    ===========
    dimensao_tuple : tuple
        Tupla com dimensões da rede neural
        
    Retorna
    =======
    rede_list : list
        Lista com matrizes da rede neural
    '''
    
    rede_list = list()
    
    for i in range(len(dimensao_tuple) - 1):   
        W_array = np.zeros([dimensao_tuple[i+1], dimensao_tuple[i]])
        B_array = np.zeros(dimensao_tuple[i+1])[:,None]    
        rede_list.append(W_array)
        rede_list.append(B_array) 

    return rede_list
