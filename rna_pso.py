#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Criado em 2019-09-14 16:00:17

Autor: Gilvan Farias da Silva
'''

#---IMPORTS-------------------------------------------------------------------#

import numpy as np

import utilidades as util

import funcoes as func

#---CLASSE-Particula----------------------------------------------------------#

class Particula:
    def __init__(self, 
                 dimensao_tuple = (3,3,3,),
                 w_float = 1,
                 c1_float = 1,
                 c2_float = 1,
                 velocidade_minima_float = -0.5,
                 velocidade_maxima_float = 0.5, 
                 posicao_minima_float = -15, 
                 posicao_maxima_float = 15,
                 inicializacao = 'nguyen',
                 otimizacao = 'minimizar',
                 tratar_restricao = 'sem_restricao',
                 otimizar_variavel = 'cross_entropia',
                 funcao_ativacao_escondida = 'tanh',  
                 funcao_ativacao_final = 'softmax',
                 tipo_perda = 'cross_entropia',
                 tipo_escore = 'acuracia',
                 factivel = False):
        '''
        Inicializa um objeto do tipo Particula
        
        Parâmetros:
        ===========
        dimensao_tuple : tuple
            Tupla com dimensões da rede neural
        w_float : float
            Constante de inércia
        c1_float : float
            Constante cognitiva
        c2_float : float
            Constante social
        velocidade_minima_float : float
            Valor da menor componente de velocidade
        velocidade_maxima_float : float
            Valor da maior componente de velocidade
        '''
        
        self.dimensao_tuple = dimensao_tuple
        self.w_float = w_float      
        self.c1_float = c1_float       
        self.c2_float = c2_float 
        self.velocidade_minima_float = velocidade_minima_float
        self.velocidade_maxima_float = velocidade_maxima_float
        self.posicao_minima_float = posicao_minima_float
        self.posicao_maxima_float = posicao_maxima_float
        self.inicializacao = inicializacao
        self.otimizacao = otimizacao
        self.tratar_restricao = tratar_restricao
        self.otimizar_variavel = otimizar_variavel
        self.funcao_ativacao_escondida = funcao_ativacao_escondida  
        self.funcao_ativacao_final = funcao_ativacao_final
        self.tipo_perda = tipo_perda
        self.tipo_escore = tipo_escore
        self.factivel = factivel
        
        self._inicializa_fitness()        
        self._inicializa_pesos()
        self.melhor_posicao_particula_list = self.posicao_list
    
    
    
    def _inicializa_fitness(self):
        if self.otimizacao == 'minimizar':
            self.valor_fitness_float = float('inf')
            self.melhor_valor_fitness_float = float('inf') 
        elif self.otimizacao == 'maximizar':
            self.valor_fitness_float = -float('inf')
            self.melhor_valor_fitness_float = -float('inf') 
    
    
    
    def _atualiza_fitness(self):
        if self.otimizacao == 'minimizar':
            if self.valor_fitness_float < self.melhor_valor_fitness_float :
                self.melhor_posicao_particula_list = self.posicao_list
                self.melhor_valor_fitness_float = self.valor_fitness_float
        elif self.otimizacao == 'maximizar':
            if self.valor_fitness_float > self.melhor_valor_fitness_float :
                self.melhor_posicao_particula_list = self.posicao_list
                self.melhor_valor_fitness_float = self.valor_fitness_float
    
    
    
    def _inicializa_pesos(self):
        if self.inicializacao == 'nguyen':
            self.posicao_list = util.init_nguyen(self.dimensao_tuple)
            #self.velocidade_list = util.init_nguyen(self.dimensao_tuple)  
            self.velocidade_list = util.init_velocidade(self.dimensao_tuple)      
        elif self.inicializacao == 'glorot':
            self.posicao_list = util.init_glorot(self.dimensao_tuple)       
            self.velocidade_list = util.init_glorot(self.dimensao_tuple)  
        elif self.inicializacao == 'he':
            self.posicao_list = util.init_he(self.dimensao_tuple)       
            self.velocidade_list = util.init_he(self.dimensao_tuple)  
        
        
        
    def reinicia(self):       
        self._inicializa_pesos()
        
        
        
    def propagar(self, entrada_array):      
        
        if len(entrada_array.shape) == 1:
            X = entrada_array[None,:]    
        else:
            X = entrada_array
        
        W0, B0, W1, B1 = self.posicao_list
    
        Y0 = func.ATIVACAO[self.funcao_ativacao_escondida](W0 @ X.T + B0)
        Y1 = func.ATIVACAO[self.funcao_ativacao_final](W1 @ Y0 + B1)

        return Y1.T
        


    def objetivo(self, entrada_array, desejado_array, iteracao_int):
        '''
        Calcula o valor da função objetivo (fitness)
        
        Parâmetros
        ==========
        obj_func : function
            Função com a lógica para calcular o  valor fitness. 
            Deve ter os seguintes parâmetros:
                (self.posicao_list, entrada_array, real_array, iteracao_int)
            Deve retornar o seguinte:
                [valor_fitness_float, matriz_confusao_array]
        entrada_array : array
            Entrada da rede neural
        real_array : array
            Saída real da rede neural
        iteracao_int : int
            Iteração atual do algoritmo
        '''
        
        if self.tratar_restricao == 'sem_restricao':
            Y = self.propagar(entrada_array)
            
        if len(desejado_array.shape) == 1:
            desejado_array = desejado_array[None,:]    
            
        if self.otimizar_variavel == 'cross_entropia':
            self.valor_fitness_float = util.cross_entropia(desejado_array, Y) 
        elif self.otimizar_variavel == 'erro_quadratico_medio':
            self.valor_fitness_float = util.erro_quadratico_medio(desejado_array, Y) 
        elif self.otimizar_variavel == 'acuracia':
            cm = util.matriz_confusao(desejado_array, Y)
            self.valor_fitness_float = util.acuracia(cm)
        
        self._atualiza_fitness()
            
        
            
    def move_particula(self, melhor_posicao_global_list):
        '''
        Move a partícula
        
        Parâmetros
        ==========
        melhor_posicao_global_list : list
            Posição da partícula com melhor valor da função objetivo
        '''
        
        r1_float = np.random.rand()
        r2_float = np.random.rand()
        
        for i in range(len(self.velocidade_list)):
            self.velocidade_list[i] = (self.w_float*self.velocidade_list[i]) +\
            (self.c1_float*r1_float) * \
            (self.melhor_posicao_particula_list[i] - self.posicao_list[i]) + \
            (self.c2_float*r2_float) * \
            (melhor_posicao_global_list[i] - self.posicao_list[i])  
            
            self.velocidade_list[i] = np.clip(self.velocidade_list[i], 
                                a_min = self.velocidade_minima_float,
                                a_max = self.velocidade_maxima_float)
            
            self.posicao_list[i] = self.posicao_list[i] +\
            self.velocidade_list[i]
            
            self.posicao_list[i] = np.clip(self.posicao_list[i],
                                a_min = self.posicao_minima_float,
                                a_max = self.posicao_maxima_float)

        
        
#---CLASSE-Enxame-------------------------------------------------------------#
            

            
class Enxame:
    def __init__(self, 
                 entrada_treino_array, 
                 desejado_treino_array, 
                 entrada_validacao_array, 
                 desejado_validacao_array, 
                 dimensao_tuple = (3,3,3,),
                 num_particulas_int = 50,
                 max_iteracoes_int = 30,
                 mensagem_bool = True, 
                 w_float = 1,
                 c1_float = 1,
                 c2_float = 1,
                 velocidade_minima_float = -0.5,
                 velocidade_maxima_float = 0.5,
                 posicao_minima_float = -15, 
                 posicao_maxima_float = 15,
                 inicializacao = 'nguyen',
                 otimizacao = 'minimizar',
                 otimizador = 'estocastico',
                 tratar_restricao = 'sem_restricao',
                 otimizar_variavel = 'cross_entropia',
                 funcao_ativacao_escondida = 'tanh',  
                 funcao_ativacao_final = 'softmax',
                 tipo_perda = 'cross_entropia',
                 tipo_escore = 'acuracia',
                 taxa_reinicializacao_float = 0.2):
        '''
        Inicializa enxame e executa algoritmo PSO
        
        Parâmetros
        ==========
        obj_func : function
            Função com a lógica para calcular o  valor fitness. 
            Deve ter os seguintes parâmetros:
                (self.posicao_list, entrada_array, real_array, iteracao_int)
            Deve retornar o seguinte:
                [valor_fitness_float, matriz_confusao_array]
        entrada_array : array
            Entrada da rede neural
        real_array : array
            Saída real da rede neural
        dimensao_tuple : tuple
            Tupla com dimensões da rede neural
        
        Parâmetros opcionais
        ====================        
        num_particulas_int : int
            Número de partículas no enxame
        max_iteracoes_int : int
            Número máximo de iterações
        mensagem_bool : bool
            Define se mensagens serão exibidas durante a execução do algoritmo
        w_float : float
            Constante de inércia
        c1_float : float
            Constante cognitiva
        c2_float : float
            Constante social
        velocidade_minima_float : float
            Valor da menor componente de velocidade
        velocidade_maxima_float : float
            Valor da maior componente de velocidade
        ''' 

        self.dimensao_tuple = dimensao_tuple
        self.num_particulas_int = num_particulas_int
        self.max_iteracoes_int = max_iteracoes_int
        self.mensagem_bool = mensagem_bool
        self.w_float = w_float
        self.c1_float = c1_float
        self.c2_float = c2_float
        self.velocidade_minima_float = velocidade_minima_float
        self.velocidade_maxima_float = velocidade_maxima_float
        self.posicao_minima_float = posicao_minima_float
        self.posicao_maxima_float = posicao_maxima_float
        self.inicializacao = inicializacao
        self.otimizacao = otimizacao
        self.otimizador = otimizador
        self.tratar_restricao = tratar_restricao
        self.otimizar_variavel = otimizar_variavel
        self.funcao_ativacao_escondida = funcao_ativacao_escondida
        self.funcao_ativacao_final = funcao_ativacao_final
        self.tipo_perda = tipo_perda
        self.tipo_escore = tipo_escore
        self.taxa_reinicializacao_float = taxa_reinicializacao_float
        
        
        self.loss_treino_list = list()
        self.loss_validacao_list = list()
        self.score_treino_list = list()
        self.score_validacao_list = list()
        self.enxame_list = list()   
        
        
        if inicializacao == 'nguyen':
            self.melhor_posicao_global_list =\
                util.init_nguyen(self.dimensao_tuple) 
        elif inicializacao == 'glorot':
            self.melhor_posicao_global_list =\
                util.init_glorot(self.dimensao_tuple) 
        
        
        if self.otimizacao == 'minimizar':
            self.melhor_valor_fitness_global_float = float('inf')
        elif self.otimizacao == 'maximizar':
            self.melhor_valor_fitness_global_float = -float('inf')
           
        
        self._mensagem('Inicializando enxame!\n')
            
        # Inicializa enxame
        for p in range(self.num_particulas_int):
            self.enxame_list.append(Particula(self.dimensao_tuple,
                 w_float=w_float, c1_float=c1_float, c2_float=c2_float,
                 otimizacao = otimizacao,
                 otimizar_variavel = otimizar_variavel,
                 velocidade_minima_float=velocidade_minima_float,                 
                 velocidade_maxima_float=velocidade_maxima_float))
        
        self._mensagem('Otimizando!\n')
            
        # Otimização
        # Para cada iteração
        for i in range(self.max_iteracoes_int):  
            
            if self.otimizador == 'estocastico':
                self._otmizador_estocastico(entrada_treino_array, desejado_treino_array, i)
            elif self.otimizador == 'batch':
                self._otmizador_batch(entrada_treino_array, desejado_treino_array, i)
            
            
            #Calcula o loss e score do treino aqui
            predito_array = self.predizer(entrada_treino_array)
            self.loss_treino_list.append(util.cross_entropia(desejado_treino_array, predito_array))
            matriz_confusao_array = util.matriz_confusao(desejado_treino_array, predito_array)
            #self.score_treino_list.append(util.acuracia(matriz_confusao_array))
            self.score_treino_list.append(self.melhor_valor_fitness_global_float)
            
            #Consertar !!!!!!!!!!!!!!!!!!!!!!!!
            
            #Calcula o loss e score de validação aqui
            '''
            predito_array = self.predizer(entrada_validacao_array)
            self.loss_validacao_list.append(util.cross_entropia(desejado_validacao_array, predito_array))
            matriz_confusao_array = util.matriz_confusao(desejado_validacao_array, predito_array)
            self.score_validacao_list.append(util.acuracia(matriz_confusao_array))
            '''
            
            self._mensagem('Iteração = '+str(i)+' Melhor fitness = '+str(self.melhor_valor_fitness_global_float)+' Score = '+str(self.score_treino_list[i]))
            
        self._mensagem('Fim\n')
        
        
        
    def _otmizador_estocastico(self, entrada_treino_array, desejado_treino_array, i):
        # Para cada leitura
        
        # Consertar essa função
        
        for r in range(len(entrada_treino_array)):                
            
            # Move Particulas
            self.move_particulas_enxame()

            # Função objetivo de todas as partículas
            for p in range(self.num_particulas_int):
                self.enxame_list[p].objetivo(entrada_treino_array[r], desejado_treino_array[r], i)
            
            # Atualiza o valor do fitness global
            self.atualiza_fitness_global()
    
    
    
    def _otmizador_batch(self, entrada_treino_array, desejado_treino_array, i):

        # Move Particulas
        self.move_particulas_enxame()

        # Função objetivo de todas as partículas
        for p in range(self.num_particulas_int):
            self.enxame_list[p].objetivo(entrada_treino_array, desejado_treino_array, i)
        
        # Atualiza o valor do fitness global
        self.atualiza_fitness_global()
    
    
    
    def move_particulas_enxame(self):
        for p in range(self.num_particulas_int):                    
            reinit = np.random.rand()
            if reinit < self.taxa_reinicializacao_float:
                self.enxame_list[p].reinicia()
            
            self.enxame_list[p].move_particula(\
                            self.melhor_posicao_global_list)
        
        
        
    def atualiza_fitness_global(self):
        if self.otimizacao == 'minimizar':
            for p in range(self.num_particulas_int):
                if self.enxame_list[p].valor_fitness_float < self.melhor_valor_fitness_global_float:
                    self.melhor_valor_fitness_global_float = self.enxame_list[p].valor_fitness_float   
                    self.melhor_posicao_global_list = self.enxame_list[p].melhor_posicao_particula_list
        elif self.otimizacao == 'maximizar':
            for p in range(self.num_particulas_int):
                if self.enxame_list[p].valor_fitness_float > self.melhor_valor_fitness_global_float:
                    self.melhor_valor_fitness_global_float = self.enxame_list[p].valor_fitness_float   
                    self.melhor_posicao_global_list = self.enxame_list[p].melhor_posicao_particula_list



    def _mensagem(self, msg_str):
        if (self.mensagem_bool):
            print(msg_str)



    def predizer(self, entrada_array):
        '''
        Calcula a saída de uma rede neural considerando a melhor rede obtida 
        pelo treinamento pelo método PSO
        
        Parâmetros
        ==========
        entrada_array : array
            Entrada da rede neural
            
        Retorna
        =======
        Y1_array : array
            Saída da rede neural
        '''
        
        W0_array, B0_array, W1_array, B1_array = self.melhor_posicao_global_list

        Y0_array = np.tanh(np.dot(W0_array, entrada_array.T) + B0_array)
        Y1_array = func.softmax(np.dot(W1_array, Y0_array) + B1_array)
        Y1_array = Y1_array.T
        
        return Y1_array

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
