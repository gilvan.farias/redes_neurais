#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Criado em 2019-09-14 16:00:17

Autor: Gilvan Farias da Silva
'''
from copy import deepcopy

import numpy as np

import utilidades as util
import funcoes as func


class Particula:
    def __init__(self,
                 dimensao_tuple=(3, 3, 3,),
                 velocidade_minima_float=-0.5,
                 velocidade_maxima_float=0.5,
                 posicao_minima_float=-2.5,
                 posicao_maxima_float=2.5,
                 inicializacao='nguyen',
                 funcao_ativacao_escondida='tanh',
                 funcao_ativacao_final='tanh',
                 factivel=False):
        '''
        Inicializa um objeto do tipo Particula

        Parâmetros:
        ===========
        dimensao_tuple : tuple
            Tupla com dimensões da rede neural
        velocidade_minima_float : float
            Valor da menor componente de velocidade
        velocidade_maxima_float : float
            Valor da maior componente de velocidade
        '''

        self.dimensao_tuple = dimensao_tuple
        self.velocidade_minima_float = velocidade_minima_float
        self.velocidade_maxima_float = velocidade_maxima_float
        self.posicao_minima_float = posicao_minima_float
        self.posicao_maxima_float = posicao_maxima_float
        self.inicializacao = inicializacao
        self.funcao_ativacao_escondida = funcao_ativacao_escondida
        self.funcao_ativacao_final = funcao_ativacao_final
        self.factivel = factivel

        self.valor_fitness_float = float('inf')
        self.p_best_fitness_float = float('inf')

        if self.inicializacao == 'nguyen':
            self.posicao_list = util.init_nguyen(self.dimensao_tuple)
            self.velocidade_list = util.init_velocidade(self.dimensao_tuple)
        elif self.inicializacao == 'glorot':
            self.posicao_list = util.init_glorot(self.dimensao_tuple)
            self.velocidade_list = util.init_glorot(self.dimensao_tuple)
        elif self.inicializacao == 'he':
            self.posicao_list = util.init_he(self.dimensao_tuple)
            self.velocidade_list = util.init_he(self.dimensao_tuple)

        self.p_best_posicao_list = deepcopy(self.posicao_list)

        self.matriz_confusao = 0
        self.acuracia_float = 0
        self.pe_float = 0
        self.eqm_float = float('inf')
        self.entropia_float = float('inf')

    def atualiza_fitness(self):

        if self.valor_fitness_float < self.p_best_fitness_float:
            #print('atualiza_fitness',
            #      self.valor_fitness_float,
            #      self.p_best_fitness_float)
            self.p_best_posicao_list = deepcopy(self.posicao_list)
            self.p_best_fitness_float = self.valor_fitness_float

    def propagar(self, entrada_array):

        if len(entrada_array.shape) == 1:
            X = entrada_array[None, :]
        else:
            X = entrada_array

        W0, B0, W1, B1 = self.posicao_list

        Y0 = func.ATIVACAO[self.funcao_ativacao_escondida](W0 @ X.T + B0)
        I1 = (W1 @ Y0 + B1)
        Y1 = func.ATIVACAO[self.funcao_ativacao_final](I1)

        return Y1.T

    def propagar_best(self, entrada_array):

        if len(entrada_array.shape) == 1:
            X = entrada_array[None, :]
        else:
            X = entrada_array

        W0, B0, W1, B1 = self.p_best_posicao_list

        Y0 = func.ATIVACAO[self.funcao_ativacao_escondida](W0 @ X.T + B0)
        Y1 = func.ATIVACAO[self.funcao_ativacao_final]((W1 @ Y0 + B1))

        return Y1.T

    def calcula_indicadores(self, entrada_array, desejado_array):
        '''
        Calcula indicadores de interesse

        Parâmetros
        ==========
        entrada_array : array
            Entrada da rede neural
        real_array : array
            Saída real da rede neural
        '''

        Y = self.propagar(entrada_array)

        if len(desejado_array.shape) == 1:
            desejado_array = desejado_array[None, :]

        self.matriz_confusao = util.matriz_confusao(desejado_array, Y)
        self.matriz_confusao_normalizada = util.matriz_confusao_normalizada(desejado_array, Y)
        self.matriz_confusao_normalizada = util.matriz_confusao_normalizada(desejado_array, Y)
        self.acuracia_float = util.acuracia(self.matriz_confusao)
        self.pe_float = util.produto_eficiencias(self.matriz_confusao_normalizada)
        self.eqm_float = util.erro_quadratico_medio(desejado_array, Y)
        self.entropia_float = util.cross_entropia(desejado_array, Y)
        self.pd = util.probabilidade_de_deteccao(self.matriz_confusao)
        self.pf = util.probabilidade_falso_alarme(self.matriz_confusao)
        self.vp = util.verdadeiro_positivo(self.matriz_confusao)
        self.fp = util.falso_positivo(self.matriz_confusao)
        self.fn = util.falso_negativo(self.matriz_confusao)
        self.vn = util.verdadeiro_negativo(self.matriz_confusao)
        self.prec = util.precisao(self.matriz_confusao)
        self.conf = util.confusao(self.matriz_confusao)
        self.erro = util.erro(self.matriz_confusao)
        self.dkl = util.dkl(desejado_array, Y)

    def calcula_indicadores_best(self, entrada_array, desejado_array):
        '''
        Calcula indicadores de interesse

        Parâmetros
        ==========
        entrada_array : array
            Entrada da rede neural
        real_array : array
            Saída real da rede neural
        '''

        Y = self.propagar_best(entrada_array)

        if len(desejado_array.shape) == 1:
            desejado_array = desejado_array[None, :]

        self.matriz_confusao = util.matriz_confusao(desejado_array, Y)
        self.matriz_confusao_normalizada = util.matriz_confusao_normalizada(desejado_array, Y)
        self.acuracia_float = util.acuracia(self.matriz_confusao)
        self.pe_float = util.produto_eficiencias(self.matriz_confusao_normalizada)
        self.eqm_float = util.erro_quadratico_medio(desejado_array, Y)
        self.entropia_float = util.cross_entropia(desejado_array, Y)
        self.pd = util.probabilidade_de_deteccao(self.matriz_confusao)
        self.pf = util.probabilidade_falso_alarme(self.matriz_confusao)
        self.vp = util.verdadeiro_positivo(self.matriz_confusao)
        self.fp = util.falso_positivo(self.matriz_confusao)
        self.fn = util.falso_negativo(self.matriz_confusao)
        self.vn = util.verdadeiro_negativo(self.matriz_confusao)
        self.prec = util.precisao(self.matriz_confusao)
        self.conf = util.confusao(self.matriz_confusao)
        self.erro = util.erro(self.matriz_confusao)
        self.dkl = util.dkl(desejado_array, Y)

    def atualiza_posicao(self, g_best_posicao_list,
                         w_float,
                         c1_float,
                         c2_float):
        '''
        Move a partícula

        Parâmetros
        ==========
        g_best_posicao_list : list
            Posição da partícula com melhor valor da função objetivo
        '''

        r1_float = np.random.rand()
        r2_float = np.random.rand()

        for i in range(len(self.velocidade_list)):
            self.velocidade_list[i] = (w_float*self.velocidade_list[i]) +\
            (c1_float*r1_float) * \
            (self.p_best_posicao_list[i] - self.posicao_list[i]) * \
            util.lclose(self.p_best_posicao_list[i], self.posicao_list[i]) + \
            (c2_float*r2_float) * \
            (g_best_posicao_list[i] - self.posicao_list[i]) * \
            util.lclose(g_best_posicao_list[i], self.posicao_list[i])

            self.velocidade_list[i] = np.clip(self.velocidade_list[i],
                                              a_min=self.velocidade_minima_float,
                                              a_max=self.velocidade_maxima_float)

            self.posicao_list[i] = self.posicao_list[i] +\
            self.velocidade_list[i]

            self.posicao_list[i] = np.clip(self.posicao_list[i],
                                           a_min=self.posicao_minima_float,
                                           a_max=self.posicao_maxima_float)

    def reinicia(self):
        if self.inicializacao == 'nguyen':
            self.posicao_list = util.init_nguyen(self.dimensao_tuple)
            self.velocidade_list = util.init_velocidade(self.dimensao_tuple)
        elif self.inicializacao == 'glorot':
            self.posicao_list = util.init_glorot(self.dimensao_tuple)
            self.velocidade_list = util.init_velocidade(self.dimensao_tuple)
        elif self.inicializacao == 'he':
            self.posicao_list = util.init_he(self.dimensao_tuple)
            self.velocidade_list = util.init_velocidade(self.dimensao_tuple)
            
    def retropropaga(self, entrada_array, desejado_array, p_anterior):
        X = entrada_array.T
        D = desejado_array.T

        mt = 4.9
        W0A, B0A, W1A, B1A = p_anterior
        W0, B0, W1, B1 = self.p_best_posicao_list
        
        IN0 = W0 @ X + B0
        Y0 = func.ATIVACAO[self.funcao_ativacao_escondida](IN0)
        IN1 = W1 @ Y0 + B1
        Y1 = func.ATIVACAO[self.funcao_ativacao_final](IN1)
        
        #print(util.erro_quadratico_medio(D.T, Y1.T))
        
        DT1 = -(Y1 - D)
        W1 += 0.001 * DT1 @ Y0.T/Y0.shape[1] + mt*(W1 - W1A)
        DT0 = (W1.T @ DT1) * func.DERIVADA[self.funcao_ativacao_escondida](IN0)
        W0 += 0.001 * DT0 @ X.T/X.shape[1] + mt*(W0 - W0A)
        
        B1 += (0.001 * np.mean(DT1+ mt*(B1 - B1A), axis=1) )[:,None]
        B0 += (0.001 * np.mean(DT0+  mt*(B0 - B0A), axis=1) )[:,None]
        
        
